package com.haisi.chefme.Login.Presenter.Interface;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Isingh930 on 10/11/17.
 */

public interface ILoginPresenter {
    void checkIfUserLoggedIn();
    void setUpFirebaseAuth();
    void attachAuthStateListener();
    void detachAuthStateListener();

}
