package com.haisi.chefme.Main.Friends.View.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.haisi.chefme.Main.Friends.Presenter.Interface.IFriendsPresenter;
import com.haisi.chefme.Main.Friends.View.Interface.IFriendsViewHolder;
import com.haisi.chefme.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class FriendRecyclerViewAdapter extends RecyclerView.Adapter<FriendRecyclerViewAdapter.FriendViewHolder>{

    private IFriendsPresenter mFriendsPresenter;

    public FriendRecyclerViewAdapter(IFriendsPresenter friendsPresenter) {
        mFriendsPresenter = friendsPresenter;
    }

    @Override
    public FriendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.friend_list_item, parent, false);
        return new FriendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FriendViewHolder holder, int position) {
        mFriendsPresenter.onBindFriendsViewHolderAtPosition(holder, position);
    }

    @Override
    public int getItemCount() {
        return mFriendsPresenter.getFriendsListSize();
    }

    public class FriendViewHolder extends RecyclerView.ViewHolder implements IFriendsViewHolder{
        @BindView(R.id.friend_name_text_view)
        TextView mFriendNameTextView;

        @BindView(R.id.friend_image_view)
        ImageView mFriendImageView;

        public FriendViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        @Override
        public void setFriendName(String friendName) {
            mFriendNameTextView.setText(friendName);
        }

        @Override
        public void setFriendImageURL(String imageURL) {
            //Use picasso to set image
        }
    }
}
