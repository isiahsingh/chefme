package com.haisi.chefme.Main.Home;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.os.ConfigurationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.haisi.chefme.Main.Home.Adapter.HomeRecyclerViewAdapter;
import com.haisi.chefme.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class HomeFragment extends Fragment {

    private Unbinder mUnbinder;
    private View mView;
    private HomeRecyclerViewAdapter mHomeRecyclerAdapter;
    private LinearLayoutManager mLinearLayoutManager;

    @BindView(R.id.home_recycler_view)
    RecyclerView mHomeRecyclerView;

    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.home_fragment, container, false);
        mUnbinder = ButterKnife.bind(this, mView);

        mHomeRecyclerAdapter = new HomeRecyclerViewAdapter();
        mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mHomeRecyclerView.setLayoutManager(mLinearLayoutManager);
        mHomeRecyclerView.setAdapter(mHomeRecyclerAdapter);

        mHomeRecyclerView.setHasFixedSize(true);

        return mView;
    }
}
