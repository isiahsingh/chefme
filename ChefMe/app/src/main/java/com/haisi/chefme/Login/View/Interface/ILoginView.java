package com.haisi.chefme.Login.View.Interface;

/**
 * Created by Isingh930 on 10/11/17.
 */

public interface ILoginView {
    void startMainActivity();
    void startRegisterActivity();
}
