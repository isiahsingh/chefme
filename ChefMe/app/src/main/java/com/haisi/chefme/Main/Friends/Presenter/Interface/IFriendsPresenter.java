package com.haisi.chefme.Main.Friends.Presenter.Interface;

import android.widget.SearchView;

import com.haisi.chefme.Main.Friends.View.Interface.IFriendsView;
import com.haisi.chefme.Main.Friends.View.Interface.IFriendsViewHolder;

/**
 * Created by Isingh930 on 10/10/17.
 */

public interface IFriendsPresenter extends SearchView.OnQueryTextListener{

    int getFriendsListSize();
    void onBindFriendsViewHolderAtPosition(IFriendsViewHolder viewHolder, int position);
    void attachView(IFriendsView view);
    void detachView();
}
