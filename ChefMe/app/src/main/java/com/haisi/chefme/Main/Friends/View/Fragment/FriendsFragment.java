package com.haisi.chefme.Main.Friends.View.Fragment;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.SearchView;

import com.haisi.chefme.Main.Friends.Presenter.Interface.IFriendsPresenter;
import com.haisi.chefme.Main.Friends.Presenter.Presenter.FriendsPresenter;
import com.haisi.chefme.Main.Friends.View.Adapter.FriendRecyclerViewAdapter;
import com.haisi.chefme.Main.Friends.View.Interface.IFriendsView;
import com.haisi.chefme.Main.MainActivity;
import com.haisi.chefme.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class FriendsFragment extends Fragment implements IFriendsView{

    private View mView;
    private IFriendsPresenter mFriendsPresenter;
    private Unbinder mUnbinder;
    private GridLayoutManager mGridLayoutManager;
    private FriendRecyclerViewAdapter mRecyclerViewAdapter;

    @BindView(R.id.friend_recycler_view)
    RecyclerView mFriendRecyclerView;

    public FriendsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.friend_fragment, container, false);
        mUnbinder = ButterKnife.bind(this, mView);

        // Presenter Dependency
        attachPresenter();
        ((MainActivity) getActivity()).setPresenterForConfigurationChange(mFriendsPresenter, "Friends");

        mGridLayoutManager = new GridLayoutManager(getContext(), 2, GridLayout.VERTICAL, false);
        mRecyclerViewAdapter = new FriendRecyclerViewAdapter(mFriendsPresenter);
        mFriendRecyclerView.setLayoutManager(mGridLayoutManager);
        mFriendRecyclerView.setAdapter(mRecyclerViewAdapter);

        return mView;
    }

    private void attachPresenter() {
        mFriendsPresenter = (IFriendsPresenter)((MainActivity)getActivity()).getPresenterForConfigurationChange("Friends");

        if (mFriendsPresenter == null) {
            mFriendsPresenter = new FriendsPresenter(this);
        }
        mFriendsPresenter.attachView(this);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);

        ActionBar actionBar = ((MainActivity)getActivity()).getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        //actionBar.setDisplayShowHomeEnabled(false);

        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(mFriendsPresenter);
        searchView.setQueryHint("Search");

    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mFriendsPresenter.detachView();
        mUnbinder.unbind();
    }


}
