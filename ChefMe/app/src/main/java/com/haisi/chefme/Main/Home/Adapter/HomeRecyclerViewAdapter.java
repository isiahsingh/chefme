package com.haisi.chefme.Main.Home.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.haisi.chefme.Models.Recipie;
import com.haisi.chefme.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class HomeRecyclerViewAdapter extends RecyclerView.Adapter<HomeRecyclerViewAdapter.HomeViewHolder> {

    private ArrayList<Recipie> mRecipieArrayList;

    public void setmRecipieArrayList(ArrayList<Recipie> recipies){
        mRecipieArrayList = recipies;
    }

    public void addToRecipies(ArrayList<Recipie> recipies){
        if(recipies != null){
            mRecipieArrayList.addAll(recipies);
        }
    }

    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.home_list_item, parent, false);

        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, int position) {
        Recipie recipie = mRecipieArrayList.get(position);

        if(recipie.getImageURL() == null){

        }
    }

    @Override
    public int getItemCount() {
        if(mRecipieArrayList == null){
            return 0;
        }

        return mRecipieArrayList.size();
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageView;

        public HomeViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.home_image_view);
        }
    }
}
