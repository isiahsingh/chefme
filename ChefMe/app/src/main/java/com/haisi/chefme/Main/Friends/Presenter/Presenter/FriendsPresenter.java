package com.haisi.chefme.Main.Friends.Presenter.Presenter;

import com.haisi.chefme.Main.Friends.Data.Interface.IFriendsRepository;
import com.haisi.chefme.Main.Friends.Data.Repository.DatabaseRepository;
import com.haisi.chefme.Main.Friends.Data.Repository.RESTRepository;
import com.haisi.chefme.Main.Friends.Presenter.Interface.IFriendsPresenter;
import com.haisi.chefme.Main.Friends.View.Interface.IFriendsView;
import com.haisi.chefme.Main.Friends.View.Interface.IFriendsViewHolder;
import com.haisi.chefme.Models.User;

import java.util.ArrayList;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class FriendsPresenter implements IFriendsPresenter {

    private ArrayList<User> mFriendsList;
    private ArrayList<User> mAllUsersList;

    private IFriendsView mFriendsView;
    private IFriendsRepository mDatabaseRepo;
    private IFriendsRepository mRESTRepo;

    public FriendsPresenter(IFriendsView friendsView){
        this.mFriendsView = friendsView;
        // Repository Dependencies
        this.mDatabaseRepo = new DatabaseRepository();
        this.mRESTRepo = new RESTRepository();
    }

    public void attachView(IFriendsView view){
        this.mFriendsView = view;
    }

    public void detachView(){
        this.mFriendsView = null;
    }

    @Override
    public int getFriendsListSize() {
        if(mFriendsList == null){
            return 0;
        }

        return mFriendsList.size();
    }

    @Override
    public void onBindFriendsViewHolderAtPosition(IFriendsViewHolder viewHolder, int position) {
        User user = mFriendsList.get(position);
        viewHolder.setFriendName(user.getName());
        viewHolder.setFriendImageURL(user.getImageURL());
    }



    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
}
