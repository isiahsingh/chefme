package com.haisi.chefme.Login.Presenter.Presenter;

import com.google.firebase.auth.FirebaseAuth;
import com.haisi.chefme.Login.Data.Interface.ILoginRepository;
import com.haisi.chefme.Login.Data.Repository.RESTLoginRepository;
import com.haisi.chefme.Login.Presenter.Interface.ILoginPresenter;
import com.haisi.chefme.Login.View.Interface.ILoginView;

/**
 * Created by Isingh930 on 10/11/17.
 */

public class LoginPresenter implements ILoginPresenter{

    private ILoginView mLoginView;
    private ILoginRepository mLoginRepo;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    public LoginPresenter(ILoginView view) {
        this.mLoginView = view;
        mLoginRepo = new RESTLoginRepository();
    }

    public void setUpFirebaseAuth(){
        mFirebaseAuth = mLoginRepo.getFirebaseAuthInstance();
        mAuthStateListener = mLoginRepo.getAuthStateListener();
    }

    public void checkIfUserLoggedIn(){
        boolean isUserLoggedIn = mLoginRepo.isUserLoggedInFirebase();
        if(isUserLoggedIn){
            mLoginView.startMainActivity();
        }else{
            mLoginView.startRegisterActivity();
        }
    }

    public void attachAuthStateListener(){
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    public void detachAuthStateListener(){
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }
}
