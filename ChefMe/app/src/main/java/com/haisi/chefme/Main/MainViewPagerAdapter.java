package com.haisi.chefme.Main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.haisi.chefme.Main.Friends.View.Fragment.FriendsFragment;
import com.haisi.chefme.Main.Home.HomeFragment;
import com.haisi.chefme.Main.Profile.ProfileFragment;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class MainViewPagerAdapter extends FragmentStatePagerAdapter {
    String[] titles = new String[]{"Friends", "Home", "Profile"};

    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new FriendsFragment();
            case 1:
                return new HomeFragment();
            case 2:
                return new ProfileFragment();
            default:
                return new HomeFragment();
        }
    }

    @Override
    public int getCount() {
        return titles.length;
    }
}
