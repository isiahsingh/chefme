package com.haisi.chefme.Main.Profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.haisi.chefme.R;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class ProfileFragment extends Fragment {

    private View mView;

    public ProfileFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.profile_fragment, container, false);


        return mView;
    }
}
