package com.haisi.chefme.Main.Friends.Data.Repository;

import com.haisi.chefme.Main.Friends.Data.Interface.IFriendsRepository;
import com.haisi.chefme.Models.User;

import java.util.ArrayList;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class DatabaseRepository implements IFriendsRepository {

    public ArrayList<User> getAllFriends(){
        return null;
    }

    public User getFriend(){
        return null;
    }

    public boolean removeFriend(){
        return false;
    }

    public boolean createFriend(User user){
        return false;
    }

}
