package com.haisi.chefme.Models;

import java.net.URL;

/**
 * Created by Isingh930 on 10/10/17.
 */

public class User {
    private String Name;
    private String ImageURL;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }
}
