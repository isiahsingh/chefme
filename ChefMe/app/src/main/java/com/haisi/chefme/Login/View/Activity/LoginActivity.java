package com.haisi.chefme.Login.View.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.haisi.chefme.BuildConfig;
import com.haisi.chefme.Login.Presenter.Interface.ILoginPresenter;
import com.haisi.chefme.Login.Presenter.Presenter.LoginPresenter;
import com.haisi.chefme.Login.View.Interface.ILoginView;
import com.haisi.chefme.Main.MainActivity;
import com.haisi.chefme.R;

import java.util.Arrays;

public class LoginActivity extends AppCompatActivity implements ILoginView{


    private static final int RC_SIGN_IN = 512;
    private ILoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mLoginPresenter = new LoginPresenter(this);
        mLoginPresenter.setUpFirebaseAuth();
        mLoginPresenter.checkIfUserLoggedIn();
//        mFirebaseAuth = FirebaseAuth.getInstance();

//        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
//            @Override
//            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                FirebaseUser user = firebaseAuth.getCurrentUser();
//                if(user != null){
//                    startMainActivity();
//                }else{
//                    startRegisterActivity();
//                }
//            }
//        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RC_SIGN_IN){
            if(resultCode == RESULT_OK){
                Toast.makeText(this, "Signed In", Toast.LENGTH_SHORT);
                startMainActivity();
            }else if(resultCode == RESULT_CANCELED){
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLoginPresenter.attachAuthStateListener();
//        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLoginPresenter.detachAuthStateListener();
//        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }


    public void startRegisterActivity(){
        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                        .setAvailableProviders(
                                Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                        new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                        .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                        .setTheme(R.style.LoginTheme)
                        .build(),
                RC_SIGN_IN);
    }

    public void startMainActivity(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
