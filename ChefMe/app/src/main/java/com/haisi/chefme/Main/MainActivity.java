package com.haisi.chefme.Main;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.haisi.chefme.Main.Friends.Presenter.Interface.IFriendsPresenter;
import com.haisi.chefme.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    public static final String ANONYMOUS = "anonymous";
    private MainPresenterWrapper mPresenterWrapper;

    private Unbinder mUnbinder;

    @BindView(R.id.main_tab_layout)
    TabLayout mTabLayout;

    @BindView(R.id.main_view_pager)
    ViewPager mViewPager;

    private String mUsername;

    //Firebase
    private FirebaseAuth mFirebaseAuth;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mUserDatabaseReference;

    private void setUpViewPager(){
        mViewPager.setAdapter(new MainViewPagerAdapter(getSupportFragmentManager()));
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(1);
    }

    private void initializeFirebase(){
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mUserDatabaseReference = mFirebaseDatabase.getReference().child("Users");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPresenterWrapper = (MainPresenterWrapper) getLastCustomNonConfigurationInstance();
        if(mPresenterWrapper == null){
            mPresenterWrapper = new MainPresenterWrapper();
        }

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        mUsername = ANONYMOUS;

        //Firebase Initializations
        initializeFirebase();

        //Setup Viewpager with TabLayout
        setUpViewPager();

    }

    public Object getPresenterForConfigurationChange(String type){
        if(type == "Friends"){
            return mPresenterWrapper.friendsPresenter;
        }
        return null;
    }

    public void setPresenterForConfigurationChange(Object presenter, String type){
        if(type == "Friends"){
            mPresenterWrapper.friendsPresenter = (IFriendsPresenter)presenter;
        }

    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return mPresenterWrapper;
    }
}
