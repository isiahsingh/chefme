package com.haisi.chefme.Login.Data.Repository;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.haisi.chefme.Login.Data.Interface.ILoginRepository;

/**
 * Created by Isingh930 on 10/11/17.
 */

public class RESTLoginRepository implements ILoginRepository {

    private boolean isUserLoggedIn;

    public RESTLoginRepository() {
    }

    public FirebaseAuth.AuthStateListener getAuthStateListener(){
       return new FirebaseAuth.AuthStateListener() {
           @Override
           public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
               FirebaseUser user = firebaseAuth.getCurrentUser();
               if (user != null) {
                   isUserLoggedIn = true;
               } else {
                   isUserLoggedIn = false;
               }
           }
       };
    }

    public FirebaseAuth getFirebaseAuthInstance(){
        return FirebaseAuth.getInstance();
    }

    public boolean isUserLoggedInFirebase(){
        return isUserLoggedIn;
    }

}
