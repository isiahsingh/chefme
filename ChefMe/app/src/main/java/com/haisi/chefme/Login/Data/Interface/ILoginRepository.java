package com.haisi.chefme.Login.Data.Interface;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by Isingh930 on 10/11/17.
 */

public interface ILoginRepository {
    boolean isUserLoggedInFirebase();
    FirebaseAuth getFirebaseAuthInstance();
    FirebaseAuth.AuthStateListener getAuthStateListener();

}
