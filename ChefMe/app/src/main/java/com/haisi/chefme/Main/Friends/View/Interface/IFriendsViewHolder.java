package com.haisi.chefme.Main.Friends.View.Interface;

/**
 * Created by Isingh930 on 10/10/17.
 */

public interface IFriendsViewHolder {
    void setFriendName(String friendName);
    void setFriendImageURL(String imageURL);
}
