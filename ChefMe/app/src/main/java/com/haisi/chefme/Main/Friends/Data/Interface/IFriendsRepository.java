package com.haisi.chefme.Main.Friends.Data.Interface;

import com.haisi.chefme.Models.User;

import java.util.ArrayList;

/**
 * Created by Isingh930 on 10/10/17.
 */

public interface IFriendsRepository {

    ArrayList<User> getAllFriends();
    User getFriend();
    boolean removeFriend();
    boolean createFriend(User user);

}
